# Meal Preparation
Now that you've learned to cook some recipes, and are hopefully experimenting with your own, you can move on to meal prep.  
one of the hardest parts of cooking for yourself is sticking to eating what you cook, and ensuring you have enough food for the 
week that isnt from a box or a can.
#### Economics of meal prep.
It hardly needs emphasis: eating food you cook makes good economic sense as you no longer include markups for labor, taxes, profit,
livery, marketing, and other mainstays of even the cheapest restaurants.  As an example, take a prepared meal of oats for breakfast.  
at 340 grams, it costs 2USD.  assuming this was only consumed 260 business days out of the year, you're still wasting more than $500 
per year on something less than $2 per uncooked kilo.
#### Environmental impact
takeaway and restaurant dining is contingent largely upon consumers ignoring the impact of the food they consume.  paper wrappers and cups are trucked 
thousands of miles in some cases to their final destination, your table (and ultimately the landfill.)  if one cup weighs 12 grams,
and you eat lunch at fat burger 260 business days per year, thats more than 3 kilos of waste just from the cup.  cooking food 
yourself means reusing flatware and cutlery, which reduces the carbon footprint of your consumption.
#### quality.
restaurants are not in business for quality, but sales.  preparing your own meals means you know exactly what went into them.
Restaurants make more money if they add more sugar, salt, and fat to their meals indiscriminately as humans are evolutionarily programmed
to seek these elements of food out.  You can improve the nutritional quality of your meals by cooking them yourself.

#### Tips:
- source ingredients from discount grocery stores, or in bulk when possible
- avoid prepared dishes or pre-made mixes.  hamburger "helper" is only helping drive revenue.
- Work to ensure youre eating seasonally, when possible.  http://eattheseasons.com/

